# Verto

Automatically version a repository using Semantic Versioning,
without worrying about manually incrementing versions in files or
git.

This project was inspired by Balena's
[versionist](https://github.com/balena-io/versionist) project, but
`verto` aims to be simpler to use, and have zero configuration.

## Getting Started

* Install verto, either by cloning this repo and compiling it using
  `cargo build --release`, or by downloading a pre-compiled binary
  (coming soon)
* In an existing repository, run `verto`. Git is required for its
  proper functioning, because it uses commit messages to calculate
  version increments.
* The following project types will have their versions incremented:
  * npm
  * rust
  * any repo containing a `version.txt` file

## Troubleshooting / Common Problems

### Version Disagreement

verto requires that all plugins (project types) specify the same version.
If you have a version.txt at version 1.0.0 and a git tag set to 0.1.0, verto
will error and exit. To fix this, make sure all detected/enabled plugins are
reporting the same version.

## Usage

```
USAGE:
    verto [FLAGS] [OPTIONS]

FLAGS:
    -n, --dry-run
    -h, --help           Prints help information
        --plugin-list    List available plugins compiled into this binary
    -V, --version        Prints version information

OPTIONS:
        --branch <branch>                     Only version this branch. If you run this on a branch other than the
                                              specified branch, verto will complain and exit non-zero
    -p, --path <path>
        --plugin-add <plugin-add>...          Plugins to force the use of, regardless of whether their identifier
                                              methods detect a match for your repository
        --plugin-remove <plugin-remove>...    Plugins to not use, even though they match your repository
        --prefix <prefix>
```
