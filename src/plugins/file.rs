use std::{
    collections::HashSet,
    default::Default,
    fs, io,
    path::{Path, PathBuf},
    str,
};

use crate::{Error, Result};
use log::debug;
use semver::Version;
use walkdir::WalkDir;

const NAME: &str = "file";

#[derive(Debug)]
pub struct Plugin {
    path: PathBuf,
    prefix: String,
    enabled: Option<bool>,
}

impl Default for Plugin {
    fn default() -> Plugin {
        Plugin {
            path: PathBuf::from(".").canonicalize().unwrap(),
            prefix: String::from(""),
            enabled: None,
        }
    }
}

impl Plugin {
    pub fn new(path: &Path, prefix: &str) -> Plugin {
        Plugin {
            path: PathBuf::from(path),
            prefix: String::from(prefix),
            enabled: None,
        }
    }

    fn paths(&self) -> Result<Vec<PathBuf>> {
        let mut out = vec![];
        for entry in WalkDir::new(&self.path) {
            let e = match entry {
                Err(e) => {
                    if let Some(inner) = e.io_error() {
                        match inner.kind() {
                            io::ErrorKind::PermissionDenied => {
                                continue;
                            }
                            _ => return Err(Error::from(e)),
                        }
                    }

                    return Err(Error::from(e));
                }
                Ok(e) => e,
            };

            if let Some(f) = e.path().file_name() {
                if f == "version.txt" {
                    out.push(e.into_path());
                }
            }
        }

        // Sort the results so we get predictable responses
        out.sort();
        debug!("file: paths: {:?}", &out);

        Ok(out)
    }
}

impl crate::Plugable for Plugin {
    fn name(&self) -> String {
        NAME.to_string()
    }

    fn prefix(&self) -> &str {
        &self.prefix
    }

    fn enable(&mut self) {
        self.enabled = Some(true);
    }

    fn disable(&mut self) {
        self.enabled = Some(false);
    }

    fn enabled(&self) -> bool {
        // If the plugin's been explicitly enabled
        if let Some(enabled) = self.enabled {
            if enabled {
                return true;
            }
        }

        // Otherwise, it's disabled
        false
    }

    fn check(&self) -> Result<bool> {
        if !self.paths()?.is_empty() {
            return Ok(true);
        }

        Ok(false)
    }

    // Calculate the current version for the repo
    fn current_version(&self) -> Result<Option<Version>> {
        let mut versions = HashSet::new();

        // Go through each file read the version out of the file
        for path in self
            .paths()
            .expect("expected one or more version.txt files")
        {
            let s = fs::read_to_string(&path)?;
            let s = s.trim();
            debug!("found version {} in {:?}", s, &path);

            // Puth the version into a set so we can detect duplicates
            versions.insert(Version::parse(&s)?);
        }

        // If we found no versions, return none
        if versions.is_empty() {
            return Ok(None);
        }

        // If there's more than one version, we don't know how to calculate a
        // current version
        if versions.len() > 1 {
            return Err(Error::Version(
                "found more than one version; ensure all version.txt files have the same version"
                    .to_string(),
            ));
        }

        Ok(versions.into_iter().next())
    }

    // Write the new version to all files found
    fn write(&self, version: &str) -> Vec<PathBuf> {
        // Go through all the paths we know about and update the version
        for path in &self
            .paths()
            .expect("expected one or more version.txt files")
        {
            // Write the new version to version.txt
            fs::write(path, &version).unwrap_or_else(|e| {
                panic!(
                    "failed to write version {} to file {:?}: {}",
                    &version, &path, e
                )
            });
            debug!("wrote version {} to {:?}", &version, &path);
        }

        self.paths().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Plugable;
    use semver::Version;
    use std::{fs, result::Result};
    use tempfile::tempdir;

    macro_rules! no_version {
        ($body:expr) => {{
            // Create a path to use for testing
            let dir = tempdir().unwrap();

            dir
        }};
    }

    macro_rules! single_version {
        ($body:expr) => {{
            // Create a path to use for testing
            let dir = tempdir().unwrap();
            let p = dir.path();

            // Write a version.txt into it
            fs::write(p.join("version.txt"), "0.0.1").unwrap();

            dir
        }};
    }

    macro_rules! multi_version {
        ($body:expr) => {{
            // Create a path to use for testing
            let dir = tempdir().unwrap();
            let p = dir.path();

            // Write a version.txt into it
            fs::write(p.join("version.txt"), "0.0.1").unwrap();

            // Write some more version.txt files
            fs::create_dir_all(dir.path().join("subdir1")).unwrap();
            fs::write(dir.path().join("subdir1/version.txt"), []).unwrap();
            fs::write(p.join("subdir1/version.txt"), "0.0.1").unwrap();

            dir
        }};
    }

    #[test]
    // Make sure that check returns false when there are no version.txt files
    fn test_check_false() {
        let dir = no_version!(1);

        // Create a plugin instance
        let plugin = self::Plugin::new(dir.path(), "");

        assert!(!plugin.check().unwrap());
    }

    #[test]
    // Make sure that check returns true when we find version.txt files, and
    // false otherwise.
    fn test_check_true() {
        let dir = single_version!(1);

        // Create a plugin instance
        let plugin = self::Plugin::new(dir.path(), "");

        assert!(plugin.check().unwrap());

        // Now create multiple versions and ensure it continues to return true
        let dir = multi_version!(1);

        // Create a plugin instance
        let plugin = self::Plugin::new(dir.path(), "");

        assert!(plugin.check().unwrap());
    }

    #[test]
    // Tests to ensure that with a single, valid version.txt the version
    // contained therein is returned
    fn test_current_version_one_version() {
        let dir = single_version!(1);

        // Create a plugin instance
        let plugin = self::Plugin::new(dir.path(), "");

        assert_eq!(
            plugin.current_version().unwrap(),
            Some(Version::parse("0.0.1").unwrap())
        )
    }

    #[test]
    // Tests to ensure that with multiple, valid versions that are the same, the
    // version gets returned
    fn test_current_version_multi_version_valid() {
        let dir = multi_version!(1);

        // Create a plugin instance
        let plugin = self::Plugin::new(dir.path(), "");

        assert_eq!(
            plugin.current_version().unwrap(),
            Some(Version::parse("0.0.1").unwrap())
        )
    }

    #[test]
    // Tests to ensure that with multiple, valid versions that are not the same,
    // an error gets returned.
    fn test_current_version_multi_version_invalid() -> Result<(), String> {
        let dir = multi_version!(1);

        // Write some more version.txt files
        fs::create_dir_all(dir.path().join("subdir2")).unwrap();
        fs::write(dir.path().join("subdir2/version.txt"), "1.0.0").unwrap();

        // Create a plugin instance
        let plugin = self::Plugin::new(dir.path(), "");

        match plugin.current_version() {
            Err(e) => match e {
                Error::Version(_) => Ok(()),
                _ => Err("expected a version error".to_string()),
            },
            Ok(_) => Err("expected an error".to_string()),
        }
    }

    #[test]
    fn test_new() {
        let dir = tempdir().unwrap();

        // Create a plugin instance
        let plugin = Plugin::new(&dir.path(), "");

        // Make sure that the path matches what we passed in
        assert_eq!(plugin.path, dir.path());
    }

    #[test]
    fn test_paths_no_file() {
        let dir = tempdir().unwrap();

        // Create a plugin instance
        let plugin = Plugin::new(&dir.path(), "");

        // Make sure that we get no paths back without any version.txt files present
        let out: Vec<PathBuf> = Vec::new();
        assert_eq!(plugin.paths().unwrap(), out);

        // Make sure calling paths() doesn't mutate the path
        assert_eq!(plugin.path, dir.path());
    }

    #[test]
    fn test_paths_one_file() {
        let dir = tempdir().unwrap();

        // Create a plugin instance
        let plugin = Plugin::new(&dir.path(), "");

        // Create a path to use for testing
        let p = dir.path().join("version.txt");

        // Now we write a version.txt file to make sure it gets picked up
        fs::File::create(&p).unwrap();
        let out = vec![p];
        assert_eq!(plugin.paths().unwrap(), out);
    }

    #[test]
    fn test_paths_multiple_files() {
        let dir = tempdir().unwrap();

        // Create a plugin instance
        let plugin = Plugin::new(&dir.path(), "");

        // Create a path to use for testing
        let p = dir.path().join("version.txt");

        // Create a version.txt in the root
        fs::File::create(&p).unwrap();

        // Write some more version.txt files
        fs::create_dir_all(dir.path().join("subdir1")).unwrap();
        fs::write(dir.path().join("subdir1/version.txt"), []).unwrap();

        // Set up our expected result. It should be sorted
        let mut out = vec![p.clone(), dir.path().join("subdir1/version.txt")];
        out.sort();
        assert_eq!(
            plugin.paths().unwrap(),
            out,
            "expect correct paths with multiple version.txt files in subdirs"
        );

        dir.close().unwrap();
    }

    #[test]
    fn test_write_one_file() {
        let dir = tempdir().unwrap();

        // Create a plugin instance
        let plugin = Plugin::new(&dir.path(), "");

        // Create a path to use for testing
        let p = dir.path().join("version.txt");

        // Now we write a version.txt file to make sure it gets picked up
        fs::File::create(&p).unwrap();
        fs::write(&p, "0.0.1").unwrap();

        plugin.write("0.0.2");

        let s = fs::read_to_string(&p).unwrap();

        assert_eq!(s, "0.0.2");
    }

    #[test]
    fn test_write_multi_file() {
        let dir = tempdir().unwrap();

        // Create a plugin instance
        let plugin = Plugin::new(&dir.path(), "");

        // Create paths to use for testing
        let p1 = dir.path().join("version.txt");
        fs::write(&p1, "0.0.1").unwrap();

        let p2 = dir.path().join("subdir1/version.txt");
        fs::create_dir_all(dir.path().join("subdir1")).unwrap();
        fs::write(&p2, "0.0.2").unwrap();

        plugin.write("0.0.3");

        let s = fs::read_to_string(&p1).unwrap();
        assert_eq!(s, "0.0.3");

        let s = fs::read_to_string(&p2).unwrap();
        assert_eq!(s, "0.0.3");
    }
}
