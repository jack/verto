use std::{
    fmt::Debug,
    path::{Path, PathBuf},
    str,
};

use crate::{Error, Result};
use chrono::Local;
use git2::{Commit, Oid, Repository, RepositoryState, Signature, Time};
use log::debug;
use regex::Regex;
use semver::Version;

const NAME: &str = "git";

pub struct Plugin {
    path: PathBuf,
    prefix: String,
    repo: Option<Repository>,
    enabled: Option<bool>,
}

impl Plugin {
    pub fn new(path: &Path, prefix: &str) -> Plugin {
        Plugin {
            path: PathBuf::from(path),
            prefix: String::from(prefix),
            repo: None,
            enabled: None,
        }
    }

    fn versions(&self) -> Result<Vec<Version>> {
        use crate::Plugable;

        let mut versions = Vec::new();

        // Get a list of all the versions tagged on the repo
        let repo = self.repo.as_ref().unwrap();
        for tag in repo.tag_names(Some("*.*.*"))?.iter() {
            // Remove any prefix here prior to running it through Version. We deal only in semvers,
            // but tags/files may have v0.0.0 formatted version strings in them
            let tag = self.remove_prefix(tag.unwrap());

            match Version::parse(&tag) {
                Ok(v) => {
                    versions.push(v);
                }
                Err(e) => {
                    debug("error parsing version", &e);
                }
            }
        }

        // Sort the versions ASC
        versions.sort();

        Ok(versions)
    }

    fn head(&self) -> Commit {
        // Get the current HEAD of the repo
        let head = self.repo.as_ref().unwrap().head().expect("cannot get HEAD");
        debug("head", &head.name());

        // Make sure it's a branch
        if !head.is_branch() {
            panic!("HEAD is not on a branch");
        }

        // Get the commit we're pointing at
        let head_commit = self
            .repo
            .as_ref()
            .unwrap()
            .find_commit(head.target().expect("unable to get HEAD target"))
            .expect("unable to get commit for HEAD");
        debug("head commit", &head_commit);

        head_commit
    }

    fn branch(&self) -> Result<String> {
        let repo = match self.repo.as_ref() {
            Some(r) => r,
            None => return Err(Error::from("unable to get repo reference")),
        };

        match repo.head()?.name() {
            Some(n) => Ok(str::replace(&String::from(n), "refs/heads/", "")),
            None => Err(Error::from("unable to get repo name")),
        }
    }

    fn commit(&self, parent: &Commit, version: &str, paths: &[&Path]) -> Result<Oid> {
        // Generate a signature to use
        let sig = self.signature();

        // Add version.txt to staged changes
        let mut index = self.repo.as_ref().unwrap().index()?;
        debug!("index length: {}\ncontents:", &index.len());

        for path in paths {
            // Get the relative path to add
            let path = path.strip_prefix(&self.path).unwrap();

            debug!("adding path {:?}", path);
            index.add_path(&path)?;
        }

        debug("index length", &index.len());
        index.write()?;
        let oid = index.write_tree()?;
        debug("oid", &oid);

        match self.repo.as_ref() {
            Some(repo) => Ok(repo.commit(
                Some("HEAD"),
                &sig,
                &sig,
                &format!("VERSION {}", version),
                &self
                    .repo
                    .as_ref()
                    .unwrap()
                    .find_tree(oid)
                    .expect("unable to find tree for index"),
                &[parent],
            )?),
            None => Err(Error::Error("unable to get repo reference".to_string())),
        }
    }

    fn signature(&self) -> Signature {
        // Open the default git config, and get the email for the current user.
        // We'll sign the commit as this user.
        let config = git2::Config::open_default().unwrap();
        let email = config.get_string(&String::from("user.email")).unwrap();

        Signature::new(
            &"verto versioning",
            &email,
            &Time::new(Local::now().timestamp(), 0),
        )
        .unwrap()
    }
}

impl crate::Plugable for Plugin {
    fn name(&self) -> String {
        NAME.to_string()
    }

    fn prefix(&self) -> &str {
        &self.prefix
    }

    fn check(&self) -> Result<bool> {
        Ok(Repository::open(&self.path).is_ok())
    }

    fn enable(&mut self) {
        self.enabled = Some(true);
    }

    fn disable(&mut self) {
        self.enabled = Some(false);
    }

    fn enabled(&self) -> bool {
        if let Some(enabled) = self.enabled {
            if enabled {
                return true;
            }
        }

        false
    }

    fn init(&mut self, branch: &Option<String>) -> Result<()> {
        self.repo = Some(Repository::open(&self.path)?);

        match self.repo.as_ref().unwrap().state() {
            RepositoryState::Clean => {
                if let Some(b) = branch {
                    if &self.branch().expect("error getting current branch") != b {
                        return Err(Error::Error(
                            "repo is not on the allowed branch!".to_string(),
                        ));
                    }
                }
            }
            state => {
                return Err(Error::Error(format!(
                    "repository not clean: state: {:?}",
                    state
                )))
            }
        }

        // Make sure this isn't a version-bump commit
        // TODO: Update this to actually detect correctly
        if let Some(name) = self.head().author().name() {
            if name == "verto versioning" {
                return Err(Error::Error(
                    "HEAD points to a branch whose last commit was a version bump".to_string(),
                ));
            }
        }

        Ok(())
    }

    fn current_version(&self) -> Result<Option<Version>> {
        let mut versions = self.versions().unwrap();

        // Grab the latest version if we can find one
        match versions.len() {
            n if n > 0 => Ok(Some(versions.pop().expect("unable to find a version"))),
            _ => Ok(None),
        }
    }

    fn next_version(&self, v: &Version) -> Result<Option<Version>> {
        // Get the commit that the latest version points to
        let latest_version_commit = self
            .repo
            .as_ref()
            .expect("unable to get ref for repo")
            .find_reference(&format!("refs/tags/{}", self.add_prefix(v)))?;

        let mut walk = self
            .repo
            .as_ref()
            .unwrap()
            .revwalk()
            .expect("error creating revwalk");

        walk.push_head().expect("error pushing HEAD onto revwalk");

        walk.hide(
            latest_version_commit
                .target()
                .expect("unable to get target for latest version commit"),
        )
        .expect("error pushing HEAD onto revwalk");

        let mut commits = Vec::new();
        for n in walk {
            let c = self.repo.as_ref().unwrap().find_commit(n.unwrap());
            commits.push(c.unwrap());
        }
        debug("commits", &commits.len());

        let new_version = calculate_new_version(v, &commits).unwrap();

        Ok(Some(new_version))
    }

    fn done(&self, version: &str, files: &[&Path]) -> Result<()> {
        let new_commit_oid = self
            .commit(&self.head(), &self.add_prefix(&version), files)
            .expect("error committing version changes");

        let new_commit = self
            .repo
            .as_ref()
            .unwrap()
            .find_commit(new_commit_oid)
            .expect("error finding newly created commit by OID");

        let tag_oid = self
            .repo
            .as_ref()
            .unwrap()
            .tag(
                &self.add_prefix(&version),
                &new_commit.as_object(),
                &self.head().author(),
                &self.add_prefix(&version),
                true,
            )
            .expect("error tagging version");

        debug("tag OID", &tag_oid);

        Ok(())
    }
}

fn debug<T: Debug>(s: &str, input: &T) {
    debug!("{}: {:?}", s, input)
}

fn calculate_new_version(version: &Version, commits: &[Commit]) -> Result<Version> {
    // build a regex to extract version keywords
    let re = Regex::new(r"(feat|fix)(\(.*\))?:").unwrap();

    let (mut major, mut minor) = (false, false);

    for c in commits {
        let summary = c.summary().unwrap();

        for cap in re.captures_iter(summary) {
            match &cap[1] {
                "major" => major = true,
                "feat" => minor = true,
                _ => {}
            }
        }
    }

    let mut new_version = version.clone();
    if major {
        new_version.increment_major()
    } else if minor {
        new_version.increment_minor()
    } else {
        new_version.increment_patch()
    };

    Ok(new_version)
}

#[cfg(test)]
mod tests {
    /* TODO: Make these tests work by creating dedicated git repos in temp dirs
    use super::*;
    use crate::Plugable;

    #[test]
    fn test_init() {
        let mut plugin = self::Plugin::new(&PathBuf::from("."), "");
        plugin.init(&None).unwrap();
    }

    #[test]
    fn test_head() {
        let mut plugin = self::Plugin::new(&PathBuf::from("."), "");
        plugin.init(&None).unwrap();
        plugin.head();
    }
    */
}
