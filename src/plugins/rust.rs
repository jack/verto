use crate::{Error, Result};
use cargo;
use jacklog::debug;
use semver::Version;
use std::{
    fs,
    path::{Path, PathBuf},
};
use toml::Value;

const NAME: &str = "rust";

pub struct Plugin {
    path: PathBuf,
    prefix: String,
    enabled: Option<bool>,
    manifest: Option<cargo::core::Manifest>,
    lockfile: Option<toml::Value>,
}

impl Plugin {
    pub fn new(path: &Path, prefix: &str) -> Plugin {
        Plugin {
            path: PathBuf::from(path),
            prefix: String::from(prefix),
            enabled: None,
            manifest: None,
            lockfile: None,
        }
    }

    fn toml_path(&self) -> PathBuf {
        PathBuf::from(self.path.as_path()).join("Cargo.toml")
    }

    fn lock_path(&self) -> PathBuf {
        PathBuf::from(self.path.as_path()).join("Cargo.lock")
    }

    /// Determine the name of the crate, as specified in Cargo.toml.
    fn crate_name(&self) -> String {
        match &self.manifest {
            Some(man) => man.name().to_string(),
            _ => panic!("unable to read name from manifest"),
        }
    }

    /// Determine the current version of the crate, as specified in Cargo.toml.
    fn crate_version(&self) -> String {
        match &self.manifest {
            Some(man) => man.version().to_string(),
            _ => panic!("unable to read version from manifest"),
        }
    }
}

impl crate::Plugable for Plugin {
    fn name(&self) -> String {
        NAME.to_string()
    }

    fn prefix(&self) -> &str {
        &self.prefix
    }

    fn enable(&mut self) {
        self.enabled = Some(true);
    }

    fn disable(&mut self) {
        self.enabled = Some(false);
    }

    fn enabled(&self) -> bool {
        if let Some(enabled) = self.enabled {
            if enabled {
                return true;
            }
        }

        false
    }

    fn check(&self) -> Result<bool> {
        // If we're missing a Cargo.toml, we can't version bump
        if fs::metadata(&self.toml_path()).is_err() {
            return Ok(false);
        }

        // If we're missing a Cargo.lock, we can't version bump
        if fs::metadata(&self.lock_path()).is_err() {
            return Ok(false);
        }

        Ok(true)
    }

    fn init(&mut self, _branch: &Option<String>) -> Result<()> {
        // Read the lockfile
        let lock = fs::read_to_string(self.lock_path()).expect("error reading Cargo.lock");
        self.lockfile = Some(
            lock.parse::<Value>()
                .expect("unable to deserialize Cargo.lock"),
        );

        // Create a default (global) cargo config object, using the defaults
        let cfg = cargo::Config::default().unwrap();

        // Read the manifest via the Cargo crate's toml utilities, destructuring to only grab the
        // manifest itself
        let (manifest, _) = cargo::util::toml::read_manifest(
            // Path to the Cargo.toml
            &self.toml_path(),
            // ?
            cargo::core::SourceId::for_path(&self.toml_path()).unwrap(),
            // Global Cargo config object
            &cfg,
        )
        .unwrap();

        // Match on the manifest, which will be an enum
        match manifest {
            cargo::core::EitherManifest::Real(man) => {
                debug!("{:#?}", man);
                self.manifest = Some(man);
            }
            cargo::core::EitherManifest::Virtual(man) => {
                debug!("{:#?}", man);
                return Err(Error::Error("unable to parse manifest".to_string()));
            }
        };

        // Get the package name from Cargo.toml
        let name = self.crate_name();

        // Get the version from Cargo.toml
        let toml_version = self.crate_version();

        // Get the version from Cargo.lock
        match &self.lockfile {
            Some(l) => {
                for pkg in l["package"].as_array().unwrap() {
                    if pkg["name"].to_string() == name && toml_version != pkg["version"].to_string()
                    {
                        return Err(Error::Error(format!(
                            "Cargo.toml has version {}, but Cargo.lock is at version {}",
                            toml_version,
                            pkg["version"].to_string()
                        )));
                    }
                }
            }
            _ => return Err(Error::Error("lockfile is None".to_string())),
        }

        Ok(())
    }

    fn current_version(&self) -> Result<Option<Version>> {
        Ok(Some(Version::parse(&self.crate_version())?))
    }

    fn write(&self, version: &str) -> Vec<PathBuf> {
        // Update Cargo.toml
        let config = fs::read_to_string(self.toml_path()).expect("error reading Cargo.toml");
        let mut config = config
            .parse::<Value>()
            .expect("unable to deserialize Cargo.toml");

        config["package"]["version"] = Value::String(version.to_string());

        // Serialize the Cargo.toml back to a string
        let config = toml::ser::to_string_pretty(&config).unwrap();

        // Write the file to disk
        fs::write(self.toml_path(), &config).expect("error writing Cargo.toml");

        // Update Cargo.lock
        let cfg = cargo::util::config::Config::default().unwrap();
        let ws = cargo::core::Workspace::new(&self.toml_path(), &cfg).unwrap();
        let (_, mut resolve) = cargo::ops::resolve_ws(&ws).unwrap();

        // Write the lockfile back to disk
        cargo::ops::write_pkg_lockfile(&ws, &mut resolve).unwrap();

        vec![self.toml_path(), self.lock_path()]
    }
}
