use std::fs;
use std::path::{Path, PathBuf};

use crate::Result;
use serde_json::Value;

const NAME: &str = "npm";

pub struct Plugin {
    path: PathBuf,
    prefix: String,
    enabled: Option<bool>,
}

impl<'a> Plugin {
    pub fn new(path: &'a Path, prefix: &str) -> Plugin {
        Plugin {
            path: PathBuf::from(path),
            prefix: String::from(prefix),
            enabled: None,
        }
    }

    fn package_path(&self) -> PathBuf {
        let mut path = PathBuf::from(&self.path);
        path.push("package.json");

        path
    }
}

impl<'a> crate::Plugable for Plugin {
    fn name(&self) -> String {
        NAME.to_string()
    }

    fn enable(&mut self) {
        self.enabled = Some(true);
    }

    fn disable(&mut self) {
        self.enabled = Some(false);
    }

    fn enabled(&self) -> bool {
        if let Some(enabled) = self.enabled {
            if enabled {
                return true;
            }
        }

        false
    }

    fn prefix(&self) -> &str {
        &self.prefix
    }

    fn check(&self) -> Result<bool> {
        Ok(match fs::metadata(&self.package_path()) {
            Ok(f) => f.is_file(),
            Err(_) => false,
        })
    }

    fn write(&self, version: &str) -> Vec<PathBuf> {
        let config = fs::read_to_string(self.package_path()).expect("error reading package.json");
        let mut config: Value =
            serde_json::from_str(&config).expect("unable to deserialize package.json");

        config["version"] = Value::String(version.to_string());

        let config =
            serde_json::to_string_pretty(&config).expect("error serializing to package.json");

        fs::write(self.package_path(), &config).expect("error writing package.json");

        vec![self.package_path()]
    }
}
