use anyhow::Result;
use jacklog::info;
use std::{path::PathBuf, process};
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(setting = structopt::clap::AppSettings::ColoredHelp)]
#[structopt(rename_all = "kebab-case")]
struct Cli {
    #[structopt(short, long)]
    /// Path to the repo to version. Defaults to current directory
    path: Option<PathBuf>,

    #[structopt(long)]
    prefix: Option<String>,

    #[structopt(long, short = "n")]
    /// Don't take any destructive actions. This won't call any methods on plugins that are expected
    /// to mutate state. Can be useful when combined with `--verbose`
    dry_run: bool,

    #[structopt(long)]
    /// List available plugins compiled into this binary
    plugin_list: bool,

    #[structopt(long)]
    /// Plugins to explicitly disable, even if the plugin detects that it should be activated for
    /// this project
    disable: Vec<String>,

    #[structopt(long, short)]
    /// Version a branch other than master. Attempting to version a branch other than master will
    /// result in an error unless this option is used
    branch: Option<String>,

    #[structopt(long, short)]
    /// Enable verbose mode. This just sets the log level to DEBUG. You can fine-tune your
    /// verbosity by setting RUST_LOG= to your desired log level
    verbose: bool,

    #[structopt(long, short)]
    /// Get the current version, but don't increment the version
    read: bool,

    #[structopt(long)]
    /// Use the version from the specified plugin, ignoring versions from other plugins
    read_from: Option<String>,

    #[structopt(long)]
    /// Use only this plugin to calculate the next version
    increment_from: Option<String>,
}

fn main() -> Result<()> {
    // Parse the input args
    let args = Cli::from_args();

    // Figure out what log level to use
    let log_level = match &args.verbose {
        true => "debug",
        false => "info",
    };
    jacklog::init(Some(&log_level))?;

    // Determine the path to the target directory, defaulting to current directory
    let path = &args.path.unwrap_or_else(|| PathBuf::from("."));
    let branch = match args.branch {
        Some(b) => Some(b),
        None => Some(String::from("master")),
    };

    // Parse the prefix to optionally add to the version
    let prefix = &args.prefix.unwrap_or_else(|| "".to_string());

    // Define closures for adding and removing any desired prefix. TODO: What is this comment
    // about?

    // Set up verto instance
    let mut verto = verto::Verto::new(path, &prefix, args.dry_run);

    // If we just want to know what plugins are available, print them and exit
    if args.plugin_list {
        for p in &verto.plugin_names() {
            println!("{}", p);
        }

        process::exit(0);
    }

    // Initialize all the plugins
    if let Err(e) = verto.initialize(&branch, &args.disable) {
        println!("{}", e);

        process::exit(1);
    }
    let current_version = match verto.current_version(&args.read_from) {
        Ok(v) => v,
        Err(e) => {
            println!("{}", e);
            process::exit(1);
        }
    };

    // If the read arg is true, just print the version and exit
    if args.read {
        println!("{}", current_version);
        process::exit(0);
    }

    let next_version = match verto.next_version(&current_version, &args.increment_from) {
        Ok(v) => v,
        Err(e) => {
            println!("{}", e);
            process::exit(1);
        }
    };
    let files = verto.write(&next_version);
    verto.commit(&next_version, &files);

    // Print the new version to stdout, so if this is running in a script it's easy to capture the
    // output
    println!("{}", &next_version);

    info!("==> done!");

    Ok(())
}
