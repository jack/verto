use std::{fmt, io};

use semver::SemVerError;

#[derive(Debug)]
pub enum VertoError {
    Version(String),
    Io(io::Error),
    Git(git2::Error),
    Error(String),
}

impl fmt::Display for VertoError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            VertoError::Version(ref err) => write!(f, "Version error: {}", err),
            VertoError::Io(ref err) => write!(f, "I/O error: {}", err),
            VertoError::Git(ref err) => write!(f, "Git error: {}", err),
            VertoError::Error(ref err) => write!(f, "Error: {}", err),
        }
    }
}

impl From<SemVerError> for VertoError {
    fn from(_e: SemVerError) -> Self {
        VertoError::Version("error parsing semver".to_string())
    }
}

impl From<io::Error> for VertoError {
    fn from(err: io::Error) -> Self {
        VertoError::Io(err)
    }
}

impl From<git2::Error> for VertoError {
    fn from(err: git2::Error) -> Self {
        VertoError::Git(err)
    }
}

impl From<walkdir::Error> for VertoError {
    fn from(err: walkdir::Error) -> Self {
        VertoError::Error(err.to_string())
    }
}

impl From<&str> for VertoError {
    fn from(err: &str) -> Self {
        VertoError::Error(String::from(err))
    }
}
