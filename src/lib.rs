use log::{debug, info};
use semver::Version;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
    result, str,
};

mod error;
mod plugins {
    // ADDING A PLUGIN: Add the module to this list
    pub mod file;
    pub mod git;
    pub mod npm;
    pub mod rust;
}

pub use error::VertoError as Error;

pub type Result<T> = result::Result<T, Error>;

// Plugable must be implemented for all plugins, and contains basic
// functionality for working with the plugin, lifecycle hooks, and providing a
// name in order to enable/disable the plugin.
pub trait Plugable {
    fn name(&self) -> String;

    /// check is run first to determine whether a give plugin should be enabled
    /// for use.
    ///
    /// # Errors
    ///
    /// When an error may be returned is determined by the specific
    /// implementation.
    fn check(&self) -> Result<bool>;
    fn prefix(&self) -> &str;
    // TODO: Figure out how to make this return itself in the builder pattern
    fn enable(&mut self);
    fn disable(&mut self);
    fn enabled(&self) -> bool;

    /// Run any required initialization code.
    ///
    /// # Errors
    ///
    /// Will never return an error by default; depends on the implementation in
    /// the implementing struct.
    fn init(&mut self, _branch: &Option<String>) -> Result<()> {
        Ok(())
    }

    // Add the prefix to the semver string
    fn add_prefix(&self, version: &dyn std::fmt::Display) -> String {
        format!("{}{}", self.prefix(), version)
    }

    /// Strip the prefix from the semver string.
    fn remove_prefix(&self, version: &str) -> String {
        version.replace(&self.prefix(), "")
    }

    /// Calculate what the current version is.
    ///
    /// # Errors
    ///
    /// Will never return an error by default; depends on the implementation in
    /// the implementing struct.
    fn current_version(&self) -> Result<Option<Version>> {
        Ok(None)
    }

    /// Calculate what the next version should be.
    ///
    /// # Errors
    ///
    /// Will never return an error by default; depends on the implementation in
    /// the implementing struct.
    fn next_version(&self, _: &Version) -> Result<Option<Version>> {
        Ok(None)
    }

    /// Write the version to file(s).
    fn write(&self, _version: &str) -> Vec<PathBuf> {
        vec![]
    }

    /// Called after all plugins have written.
    ///
    /// # Errors
    ///
    /// Will never return an error by default; depends on the implementation in
    /// the implementing struct.
    fn done(&self, _version: &str, _changed_files: &[&Path]) -> Result<()> {
        Ok(())
    }
}

pub struct Verto {
    dry_run: bool,
    plugins: Vec<Box<dyn Plugable>>,
}

impl Verto {
    pub fn new(path: &PathBuf, prefix: &str, dry_run: bool) -> Verto {
        let mut v = Verto {
            dry_run,
            plugins: Vec::new(),
        };

        // Create a canonical path, which will normalize the path and make it absolute.
        // This avoids issues later with path manipulation in plugins.
        let path = path.canonicalize().expect("error getting canonical path");

        // ADDING A PLUGIN: Add an entry here to push your plugin onto the `plugins` vec
        v.plugins = vec![
            Box::new(plugins::file::Plugin::new(&path, &prefix)),
            Box::new(plugins::rust::Plugin::new(&path, &prefix)),
            Box::new(plugins::npm::Plugin::new(&path, &prefix)),
            // It's important the git plugin go last, so all the other plugin output gets committed
            Box::new(plugins::git::Plugin::new(&path, &prefix)),
        ];

        v
    }

    /// Get a list of string names of plugins that are available.
    pub fn plugin_names(&self) -> Vec<String> {
        self.plugins
            .iter()
            .map(|x| x.name())
            .collect::<Vec<String>>()
    }

    /// # Errors
    ///
    /// Will return an error if plugin initialization fails for a plugin.
    pub fn initialize(
        &mut self,
        branch: &Option<String>,
        disabled_plugins: &[String],
    ) -> Result<()> {
        debug!("plugins disabled: {:?}", disabled_plugins);

        // Go through all the available plugins, and filter them based on
        // whether they're explicitly enabled/disabled, and whether the plugin
        // _thinks_ it should be enabled, based on whatever detection heuristics
        // it implements.
        for plugin in &mut self.plugins {
            // If the plugin has not detected that it's relevant for this project, disable it and
            // continue
            if !plugin.check()? {
                debug!("==> {} not detected; disabling", &plugin.name());
                plugin.disable();
                continue;
            }

            // Check to see whether we find the plugin name in the list of disabled plugins
            let mut found = false;
            for dp in disabled_plugins.iter() {
                if plugin.name() == *dp {
                    found = true;
                    break;
                }
            }

            // If we found the plugin name in the list of disabled plugins, disable the plugin and
            // continue
            if found {
                debug!("==> {} disabled", &plugin.name());
                plugin.disable();
                continue;
            }

            // Otherwise, it wasn't disabled and checked as valid
            debug!("==> {} detected; enabling", &plugin.name());
            plugin.enable();
        }

        info!(
            "==> plugins enabled: {}",
            self.plugins
                .iter()
                .filter_map(|p| {
                    if !p.enabled() {
                        return None;
                    }

                    Some(p.name())
                })
                .collect::<Vec<String>>()
                .join(", ")
        );

        // Initialize all the plugins
        for plugin in &mut self.plugins {
            if !plugin.enabled() {
                continue;
            }

            plugin.init(branch)?;
        }

        Ok(())
    }

    /// # Errors
    ///
    /// Will return an error if enabled plugins detect different current versions.
    /// This probably means that various files on disk and git history have gotten
    /// out of sync.
    pub fn current_version(&self, plugin_name: &Option<String>) -> Result<Version> {
        if let Some(name) = plugin_name {
            info!("==> only incrementing version using plugin: {}", &name);
        }

        // Calculate the current version
        let mut current_versions = HashMap::new();
        for plugin in &self.plugins {
            if !plugin.enabled() {
                continue;
            }

            if let Some(name) = plugin_name {
                if name != &plugin.name() {
                    continue;
                }
            }

            if let Some(v) = plugin.current_version()? {
                info!("{} at version {}", plugin.name(), v);
                current_versions.insert(plugin.name(), v);
            }
        }
        debug!("found current versions: {:#?}", &current_versions);

        // Now that we have a set of current_versions from the plugins, confirm that they're all indicating the
        // same version
        let mut current_versions: Vec<&Version> = current_versions.values().collect();
        if current_versions.is_empty() {
            return Ok(Version::parse("0.0.0").expect("unable to parse default version"));
        }
        current_versions.sort();
        current_versions.dedup();
        if current_versions.is_empty() {
            return Err(Error::from(
                "different current versions detected by different plugins;
check that all tags have been pushed, and that you've pulled the latest tags.
If they still don't match, make sure that you ran a build to update any
lockfiles if you bumped the version by hand somewhere",
            ));
        }
        let current_version = current_versions[0];
        info!("==> current version: {}", &current_version.to_string());

        Ok(current_version.clone())
    }

    /// # Errors
    ///
    /// Will return an error if no enabled plugin returned a next version or
    /// different next versions were returned by different plugins.
    pub fn next_version(
        &self,
        current_version: &Version,
        plugin_name: &Option<String>,
    ) -> Result<Version> {
        if let Some(name) = plugin_name {
            info!("==> only using version from plugin: {}", &name);
        }

        // Calculate the next version
        let mut next_versions = HashMap::new();
        for plugin in &self.plugins {
            if !plugin.enabled() {
                continue;
            }

            if let Some(name) = plugin_name {
                if name != &plugin.name() {
                    continue;
                }
            }

            if let Some(v) = plugin.next_version(current_version)? {
                next_versions.insert(plugin.name(), v);
            }
        }
        debug!("calculated next versions: {:?}", &next_versions);

        // Now that we have a set of next_versions from the plugins, confirm that they're all indicating the
        // same version
        let mut next_versions: Vec<&Version> = next_versions.values().collect();
        if next_versions.is_empty() {
            return Err(Error::from("no enabled plugin returned a next version"));
        }
        next_versions.sort();
        next_versions.dedup();
        if next_versions.len() > 1 {
            return Err(Error::from(
                "different next version returned by different plugins",
            ));
        }
        let next_version = next_versions[0];
        info!("==> next version: {}", &next_version.to_string());

        Ok(next_version.clone())
    }

    pub fn write(&mut self, next_version: &Version) -> Vec<PathBuf> {
        // Set up a vec of files that we'll use to collect the files to be updated by the
        // various plugins (and that will therefore need to be committed with the new version)
        let mut files: Vec<PathBuf> = Vec::new();

        // Write the version out to various places
        for plugin in &self.plugins {
            if !plugin.enabled() {
                continue;
            }

            debug!("==> running next_version for {}", &plugin.name());

            // Don't do anything if this is a dry-run
            if self.dry_run {
                continue;
            }

            // The `write` method should return a list of files that were updated by
            // the plugin. We need to keep track of them so we can commit them at the
            // end.
            files.append(&mut plugin.write(&next_version.to_string()))
        }

        files
    }

    pub fn commit<T: AsRef<Path>>(&mut self, next_version: &Version, files: &[T]) {
        // If we're running in dry-run mode, just exit here
        if self.dry_run {
            return;
        }

        let mut f: Vec<&Path> = Vec::new();
        for file in files {
            f.push(file.as_ref());
        }

        // Don't do anything if this is a dry-run
        for plugin in &self.plugins {
            if !plugin.enabled() {
                continue;
            }

            // After all the plugins have done their thing, we call a done hook to allow cleanup or
            // final actions like the git plugin committing changes
            plugin.done(&next_version.to_string(), &f).unwrap();
        }
    }
}
