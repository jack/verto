.DEFAULT_GOAL := build

t ?=

.PHONY: build
build: target/doc
	cargo build

.PHONY: test
test:
	cargo test --no-fail-fast $(t) -- --nocapture

.PHONY: release
release: test
	cargo build --release

target/doc: Cargo.lock Cargo.toml
	cargo doc

.PHONY: readme
readme:
	cargo run -- --help > README.txt

.PHONY: install
install: test
	cargo install --path .

.PHONY: clean
clean:
	rm -rf \
		target \
		dist

.PHONY: verto
verto:
	cargo run

.PHONY: lint
lint:
	cargo +nightly clippy -- -Wclippy::pedantic

.PHONY: dry
dry:
	cargo run -- --dry-run --verbose
